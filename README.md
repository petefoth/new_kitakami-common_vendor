# Shared vendor blobs for the Sony `kitakami` platform

Vendor shared by the Sony Xperia Z5 devices which share the kitakami plaform:, Z5 `sumire`, Z5 Compact `suzuran`, Z5 Premium `satsuki` 

## Refactoring of upstream repo structure

The [upstream repo](https://github.com/Professor-Berni/proprietary_vendor_sony) stores vendor files in three device-specific durectories, and one directory for files common to all three devices. Manifests which use the repo map it to `vendor/sony` in the source tree. This causes `duplicate path` errors when building alongside devices which take vendor files from the `TheMuppets/proprietary_vendor_sony`, repo which is also mapped to `vendor/sony`.

To avoid these problems,while allowing for automatic mirroring of upstream to continue, the files from this repo needed to build for Z5 Compact `suzuran` , have been refactored into two separate repos , this one and `new_kitakami-common_vendor` `new_suzuran_vendor_repo`. In this refactoring, the needed files from the `suzuran` directory are moved to the top level, and the other directories deleted. This has been done **only in the `v1-q` branch** , created from the upstream `lineage-17.1` branch. 

In future, to build for the next Android version, the same refactoring will have to be applied in the new `v1-r` branch to the upstream  `lineage-18.1` branch. 
